FROM golang:1.14.2-alpine3.11 as build

RUN apk --no-cache add git make musl-dev gcc \
	&& go get -d -v github.com/google/huproxy \
	&& go get -d -v github.com/BurntSushi/toml \
	&& cd src/github.com/google/huproxy \
    && go build

FROM alpine:3.11
RUN apk -U add openssl
COPY --from=build /go/src/github.com/google/huproxy/huproxy /usr/local/bin/huproxy
EXPOSE 8086
ENTRYPOINT ["/usr/local/bin/huproxy"]
CMD ["-listen", "0.0.0.0:8086"]

